# senior-frontend-code-challenge

## Divisibility test 
This is a coding challange by ZkSystems.io that tests on divisibility of a prime number

### Language used
- Javascript
- React (Ui)

### How to Test
- Clone the repository
- `cd divisibility` to go into the files
- Run `npm install` or `yarn`
- Run `npm start` or `yarn start` when the packages finish installing
- Go to `http://localhost:3000`


### Screenshot
![landing](./image.png)
