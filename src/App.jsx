import React, { useState, useEffect } from "react";
import { debounce } from "lodash";
import ThumbContainer from "./components/ThumbContainer";
import Input from "./components/Input";
import { checkDivisibilityInteger } from "./utils";

function App() {
  const [inputValue, setInputValue] = useState("");
  const [resultValue, setResultValue] = useState("");
  const [showThumb, setShowThumb] = useState(null);
  const [showInputError, setInputError] = useState(false);

  useEffect(() => {
    if (inputValue === "" || showThumb === "down" || showThumb === null) {
      setResultValue("");
    }
    if (!inputValue) {
      setShowThumb(null);
    }
  }, [inputValue, showThumb]);

  useEffect(() => {
    // reset
    if (inputValue && !resultValue) {
      setInputError(true);
    } else {
      setInputError(false);
    }
  }, [inputValue, resultValue]);

  const handleRValue = (value) => {
    // perform rValue calculation and set result
    let rValue = checkDivisibilityInteger(value);
    if (rValue) {
      setResultValue(rValue);
      setShowThumb("up");
    } else {
      setShowThumb("down");
    }
  };
  // delay execution of value by 500ms
  const getRValue = debounce((value) => handleRValue(value), 500);

  const handleOnChange = (e) => {
    setInputValue(e.target.value);
    // checks if the value is a number
    let toNumber = parseInt(e.target.value, 10);
    if (!isNaN(toNumber)) {
      return getRValue(toNumber);
    } else {
      setShowThumb(null);
    }
  };

  return (
    <div className="main">
      <div className="container">
        <div className="thumb">
          <ThumbContainer showThumb={showThumb} />
        </div>
        <div className="inputs">
          <div className="input-item">
            <label id="input-value">Input</label>
            <Input
              value={inputValue}
              onChange={handleOnChange}
              inputError={showInputError}
              disabled={false}
              aria="input-value"
            />
          </div>
          <div className="input-item">
            <label id="result-value">Output</label>
            <Input disabled value={resultValue} aria="result-value" />
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
