import React from "react";
import {
  render,
  screen,
  fireEvent,
  findByText,
  waitFor,
} from "@testing-library/react";

import App from "../App";

test("displays correct values for good primes", async () => {
  render(<App />);
  const inputNode = screen.getByLabelText("Input");
  const resultNode = screen.getByLabelText("Output");
  const value = "53";
  fireEvent.change(inputNode, { target: { value } });
  await waitFor(() => expect(resultNode.value).toEqual("13"));
});

test("doesn't display result with incorrect value", async () => {
  render(<App />);
  const inputNode = screen.getByLabelText("Input");
  const resultNode = screen.getByLabelText("Output");
  const value = "4";
  fireEvent.change(inputNode, { target: { value } });
  await waitFor(() => expect(resultNode.value).toEqual(""));
});

test("show input error with bad inputs", async () => {
  render(<App />);
  const inputNode = screen.getByLabelText("Input");
  const value = "wrong";
  fireEvent.change(inputNode, { target: { value } });
  await waitFor(() => expect(inputNode.classList.contains("error")).toBe(true));
});
