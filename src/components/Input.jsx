import React from "react";
import PropTypes from "prop-types";

const Input = ({ value, onChange, disabled, inputError, aria }) => {
  return (
    <input
      className={`input ${inputError ? "error" : ""}`}
      type="text"
      value={value}
      onChange={onChange}
      disabled={disabled}
      aria-labelledby={aria}
    />
  );
};

export default Input;

Input.propTypes = {
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  onChange: PropTypes.func,
  disabled: PropTypes.bool.isRequired,
  inputError: PropTypes.bool,
  id: PropTypes.string,
};

Input.defaultProps = {
  inputError: false,
  onChange: () => {},
  aria: "",
};
