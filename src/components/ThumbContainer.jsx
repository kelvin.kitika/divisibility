import React from "react";
import PropTypes from "prop-types";
import ThumbsUp from "./ThumbsUp";
import ThumbsDown from "./ThumbsDown";

const ThumbContainer = ({ showThumb }) => {
  if (showThumb === null) {
    return <div className="thumb-container" />;
  }
  return (
    <div className={`thumb-container ${showThumb === "up" ? "green" : "red"}`}>
      {showThumb === "up" ? <ThumbsUp /> : <ThumbsDown />}
    </div>
  );
};

export default ThumbContainer;

ThumbContainer.propTypes = {
  showThumb: PropTypes.string,
};

ThumbContainer.defaultProps = {
  showThumb: null,
};
