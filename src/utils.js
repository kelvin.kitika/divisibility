/**
 * Checks the number of tuples r to be summed for a number to be divisible by p
 * @param {number} p - prime number
 * @returns {number} r - number of tuples for a number n to be visible by p
 */
export function checkDivisibilityInteger(p) {
  let r = 1;
  if (p < 3 || p === 5) {
    return false;
  } else if (p > 999983) {
    return false;
  } else if (!isPrime(p)) {
    return false;
  } else {
    while (true) {
      // From 10^r mod p = 1 using fast modular exponential
      let testNumber = fastModularExponentiation(10, r, p);
      if (testNumber === 1) {
        return r;
      } else if (isNaN(testNumber)) {
        return testNumber;
      } else {
        r += 1;
      }
    }
  }
}

/**
 * Checks if num is a prime number
 * @param {number} num - any number
 */
function isPrime(num) {
  for (let i = 2; i < num; i++) if (num % i === 0) return false;
  return num > 1;
}

// src: https://gist.github.com/krzkaczor/0bdba0ee9555659ae5fe
/**
 * Fast modular exponentiation for a ^ b mod n
 * @returns {number}
 */
function fastModularExponentiation(a, b, n) {
  a = a % n;
  let result = 1;
  let x = a;
  while (b > 0) {
    let leastSignificantBit = b % 2;
    b = Math.floor(b / 2);

    if (leastSignificantBit == 1) {
      result = result * x;
      result = result % n;
    }

    x = x * x;
    x = x % n;
  }
  return result;
}
